import { AnyAction, combineReducers, configureStore, ThunkAction, ThunkDispatch } from '@reduxjs/toolkit';
import axios from 'axios';
import React, { useEffect } from 'react';
import './App.css';
import { Provider, useDispatch } from 'react-redux';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { NavBar } from './core/components/NavBar';
import { useInterceptor } from './core/hooks/useInterceptor';
import { getCart } from './core/store/cart/cart.actions';
import { cartStore } from './core/store/cart/cart.store';
import { httpStatusStore } from './core/store/httpStatus/http-status.store';
import CartPage from './pages/cart/CartPage';
import { productsStore } from './pages/cms/store/products.store';
import { CounterPage } from './pages/counter/CounterPage';
import { counterReducers } from './pages/counter/store';
import { counterConfigReducer } from './pages/counter/store/counter-config.reducer';
import { counterReducer } from './pages/counter/store/counter/counter.reducer';
import { CMSPage } from './pages/cms/CMSPage';
import OrderPage from './pages/order/OrderPage';
import ShopPage from './pages/shop/ShopPage';
import Shop2Page from './pages/shop2/Shop2Page';
import { catalogApi } from './pages/shop2/store/shop.api';

const rootReducer = combineReducers({
  counter: counterReducers,
  catalog: productsStore.reducer,
  [catalogApi.reducerPath]: catalogApi.reducer,
  cart: cartStore.reducer,
  httpStatus: httpStatusStore.reducer,
})

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat([
    catalogApi.middleware
  ])
})

export type RootState = ReturnType<typeof rootReducer>;
export type AppThunk = ThunkAction<void, RootState, null, AnyAction>
export type AppDispatch = ThunkDispatch<RootState, any, AnyAction>
export const useAppDispatch = () => useDispatch<AppDispatch>()

function App() {

  return (
    <BrowserRouter>
      <Provider store={store}>
        <MyApp />
      </Provider>
    </BrowserRouter>
  );
}

function MyApp() {
  const dispatch = useAppDispatch()
  useInterceptor();

  useEffect(() => {
    dispatch(getCart())
  }, [])

  return  <>
    <NavBar />
    <div className="container mt-3">
      <Routes>
        <Route path="/" element={ <div>home</div> } />
        <Route path="shop" element={ <ShopPage /> } />
        <Route path="order" element={ <OrderPage /> } />
        <Route path="shop2" element={ <Shop2Page /> } />
        <Route path="cart" element={ <CartPage /> } />
        <Route path="cms" element={ <CMSPage /> } />
        <Route path="counter" element={ <CounterPage /> } />
      </Routes>
    </div>
  </>
}

export default App;


