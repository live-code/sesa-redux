import axios from 'axios';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setHttpStatus } from '../store/httpStatus/http-status.store';

export function useInterceptor() {
  const dispatch = useDispatch();
  useEffect(() => {
    /*axios.interceptors.request.use(function (config) {
      return config;
    }, function (error) {
    });*/

    axios.interceptors.response.use(function (response) {
      console.log('SUCCESSO!', response)
      return response;
    }, function (error) {
      console.log('ERRORE2!', error)
      dispatch(setHttpStatus({ status: 'error', actionType: error.message }))

      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      return Promise.reject(error);
    });

  }, [])
}
