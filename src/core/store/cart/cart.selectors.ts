import { RootState } from '../../../App';

export const selectCart = (state: RootState) => state.cart;

export const selectTotalItemsCart = (state: RootState) =>
  state.cart.reduce((acc, cartItem) => acc + cartItem.qty, 0)

export const selectTotalCartCost = (state: RootState) =>
  state.cart.reduce((acc, cartItem) => acc + (cartItem.qty * cartItem.item.price), 0)
