import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { CartItem } from '../../../model/cart-item';
import { Product } from '../../../model/product';

export const cartStore = createSlice({
  name: 'cart',
  initialState: [] as CartItem[],
  reducers: {
    getCartSuccess(state, action: PayloadAction<CartItem[]>) {
      return action.payload
    },
    addToCartSuccess(state, action: PayloadAction<CartItem>) {
      state.push(action.payload);
    },
    editCartItemSuccess(state, action: PayloadAction<CartItem>) {
      const item = state.find(p => p.id === action.payload.id)
      if (item) {
        item.qty = action.payload.qty;
      }
    },
    removeFromCartSuccess(state, action: PayloadAction<number>) {
      const index = state.findIndex(p => p.id === action.payload);
      state.splice(index, 1)
    },
  },

})

export const {
  getCartSuccess,
  addToCartSuccess,
  editCartItemSuccess,
  removeFromCartSuccess,
} = cartStore.actions
