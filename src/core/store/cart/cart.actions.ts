import axios from 'axios';
import { AppThunk } from '../../../App';
import { CartItem } from '../../../model/cart-item';
import { Product } from '../../../model/product';
import { addToCartSuccess, editCartItemSuccess, getCartSuccess, removeFromCartSuccess } from './cart.store';

export const getCart = (): AppThunk => async dispatch => {
  try {
    const res = await axios.get<CartItem[]>('http://localhost:3001/cart')
    dispatch(getCartSuccess(res.data))
  } catch(err) {

  }

}

export const addToCart = (product: Product): AppThunk => async (dispatch) => {
  try {
    const cartItem = await axios.get<CartItem>('http://localhost:3001/cart/' + product.id)
    dispatch(editQtyCartItem(cartItem.data))
  } catch (err) {
    dispatch(addNewItemToCart(product))
  }
}

export const addNewItemToCart = (product: Product): AppThunk => async dispatch => {
  const res = await axios.post<CartItem>('http://localhost:3001/cart', {
    item: product,
    qty: 1
  })
  dispatch(addToCartSuccess(res.data))
}

export const editQtyCartItem = (cartItem: CartItem): AppThunk => async dispatch => {
  const newCartItem = await axios.patch<CartItem>('http://localhost:3001/cart/' + cartItem.id, {
    ...cartItem,
    qty: cartItem.qty + 1
  })
  dispatch(editCartItemSuccess(newCartItem.data))
}

export const deleteFromCart = (id: number): AppThunk => async (dispatch) => {
  await axios.delete<Product>('http://localhost:3001/cart/' + id)
  dispatch(removeFromCartSuccess(id))
}
