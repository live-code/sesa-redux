// core/store/http-status.store.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { getProducts } from '../../../pages/cms/store/products.actions';

interface HttpStatus {
  status: 'error' | 'success' | null,
  actionType: string | null
}

export const httpStatusStore = createSlice({
  name: 'httpStatus',
  initialState: { status: null, actionType: null } as HttpStatus,
  reducers: {
    setHttpStatus(state, action: PayloadAction<HttpStatus>) {
      state.status = action.payload.status;
      state.actionType = action.payload.actionType;
    }
  },

})

export const {
  setHttpStatus
} = httpStatusStore.actions;
