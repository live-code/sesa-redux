import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { usePrefetch } from '../../pages/shop2/store/shop.api';
import { selectTotalCartCost, selectTotalItemsCart } from '../store/cart/cart.selectors';

export const NavBar = () => {
  const cartTotalItems = useSelector(selectTotalItemsCart);
  const cartTotalCost = useSelector(selectTotalCartCost);
  const prefetchUsers = usePrefetch('getProducts')
  const getProductById = usePrefetch('getProductById')

  return <nav className="navbar navbar-expand-lg bg-light">
    <div className="container-fluid">
      <NavLink className="navbar-brand" to="/">SESA</NavLink>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink className="nav-link" to="cms">Cms</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="counter">Counter</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="shop">Shop</NavLink>
          </li>
          <li className="nav-item" onMouseOver={() => {
            prefetchUsers()
            getProductById(1)
          }}>
            <NavLink className="nav-link" to="shop2">Shop2</NavLink>
          </li>

          <li className="nav-item pull-right">
            <NavLink to="cart" className="nav-link bg-dark px-3 py-1 rounded-5 text-white">
              Cart ({cartTotalItems} €{cartTotalCost})
            </NavLink>
          </li>
        </ul>
      </div>
    </div>
  </nav>

}
