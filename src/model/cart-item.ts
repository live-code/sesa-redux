import { Product } from './product';

export interface CartItem {
  id: number;
  item: Product;
  qty: number;
}
