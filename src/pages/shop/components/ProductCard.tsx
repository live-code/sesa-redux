import { useState } from 'react';
import { Product } from '../../../model/product';

interface ProductCardsProps {
  product: Product;
  onAddToCart: (p: Product) => void;
}

export default function ProductCard (props: ProductCardsProps) {
  const [open, setOpen] = useState<boolean>(false)
  const { product: p } = props;

  return (
    <div className="card" style={{ width: '33%' }}>
      <div className="card-body">
        <h5 className="card-title d-flex justify-content-between">
          <div>{p.title}</div>
          <i className="fa fa-arrow-down" onClick={() => setOpen(s => !s)}></i>
        </h5>
        <h6 className="card-subtitle mb-2 text-muted">€ {p.price}</h6>
        {
          open && <p className="card-text">Some quick example text to build on the card title and make up the bulk of the
            card's content.</p>
        }
        <div
          className="card-link"
          onClick={() => props.onAddToCart(p)}
        >ADD TO CART</div>
      </div>
    </div>
  )
};

