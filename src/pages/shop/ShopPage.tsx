import axios from 'axios';
import { useEffect, useState } from 'react';
import { useAppDispatch } from '../../App';
import { addToCart } from '../../core/store/cart/cart.actions';
import { Product } from '../../model/product';
import ProductCard from './components/ProductCard';

export default function ShopPage () {
  const [products, setProducts] = useState<Product[]>([])
  const [error, setError] = useState(false)
  const dispatch = useAppDispatch();

  useEffect(() => {
    axios.get('http://localhost:3001/products')
      .then(res => setProducts(res.data))
      .catch(() => setError(false))
  }, [])

  function addToCartHandler(p: Product) {
    dispatch(addToCart(p))
  }

  return <div>
    <div className="d-flex flex-wrap">
      { error && <div className="alert alert-danger">error!</div>}
      {/*FILTERS*/}

      {/*PRODUCT LIST*/}
      {
        products.map(p => <ProductCard product={p} key={p.id} onAddToCart={addToCartHandler} />)
      }

      {/*PAGINATION*/}
    </div>

  </div>
};

