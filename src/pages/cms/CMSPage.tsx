import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { RootState, useAppDispatch } from '../../App';
import { addProduct, deleteProduct, getProducts } from './store/products.actions';

export const CMSPage = () => {
  const dispatch = useAppDispatch();
  const products = useSelector((state: RootState) => state.catalog.list);
  const error = useSelector((state: RootState) => state.catalog.error);
  const pending = useSelector((state: RootState) => state.catalog.pending);

  useEffect( () => {
    dispatch(getProducts())
  }, [])


  function addHandler() {
    dispatch(addProduct({
      title:'item ' + Math.random(),
      price: 10,
    }));
  }
  return <div>
    catalog Page
    { error && <div className="alert alert-danger">{error}</div>}
    { pending && <div>loading...</div> }
    <ErrorMsg />
    <button onClick={addHandler}>ADD</button>

    {
      products.map(p => {
        return <li key={p.id}>
          {p.title} - {p.id}
          <button onClick={() => dispatch(deleteProduct(p.id))}>Delete</button>
        </li>
      })
    }
  </div>
}


function ErrorMsg(props: any) {
  const httpStatus = useSelector((state: RootState) => state.httpStatus)

  return <>
    {
      (httpStatus.status === 'success' && httpStatus.actionType === 'get')
      && <div className="alert alert-success">Get Product successfully!!!</div>
    }

    {
      (httpStatus.status === 'error' && httpStatus.actionType === 'get')
      && <div className="alert alert-danger">Get Product Failed</div>
    }

    {
      (httpStatus.status === 'error' && httpStatus.actionType === 'put')
      && <div className="alert alert-danger">Get Product Failed</div>
    }

  </>
}
