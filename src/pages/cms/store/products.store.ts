import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Product } from '../../../model/product';
import { getProducts } from './products.actions';

export const productsStore = createSlice({
  name: 'products',
  initialState: {
    list: [] as Product[],
    error: '',
    pending: false,
  } ,
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      state.error = '';
      state.list = action.payload
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.error = '';
      state.list.push(action.payload);
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      state.error = '';
      const index = state.list.findIndex(p => p.id === action.payload);
      state.list.splice(index, 1)
    },
    toggleVisibilityProduct(state, action: PayloadAction<Product>) {

    },
    setError(state, action: PayloadAction<string>) {
      state.error = action.payload;
    }
  },
  extraReducers: builder => builder
    .addCase(getProducts.pending, (state, action) => {
      state.pending = true;
    })
    .addCase(getProducts.fulfilled, (state, action) => {
      state.pending = false;
      state.error = '';
      state.list = action.payload
    })
    .addCase(getProducts.rejected, (state, action) => {
      state.pending = false;
      state.error = 'get product failed!';
    })

})

export const {
  getProductsSuccess,
  addProductSuccess,
  setError,
  deleteProductSuccess,
  toggleVisibilityProduct
} = productsStore.actions
