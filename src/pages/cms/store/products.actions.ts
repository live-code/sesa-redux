import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { AppThunk } from '../../../App';
import { setHttpStatus } from '../../../core/store/httpStatus/http-status.store';
import { Product } from '../../../model/product';
import { addProductSuccess, deleteProductSuccess, getProductsSuccess, setError } from './products.store';

export const getProductsSimpleThunk = (): AppThunk => async dispatch => {
  try {
    const res = await axios.get('http://localhost:3001/products')
    dispatch(getProductsSuccess(res.data))
  } catch(err) {
    dispatch(setError('Product get failed'))
    // dispatch(setHttpStatus({ status: 'error', actionType: 'get' }))
  }
}

export const getProducts = createAsyncThunk<Product[], void>(
  'products/get',
  async (payload, { dispatch, rejectWithValue })  => {
    try {
      const response = await axios.get<Product[]>('http://localhost:3001/products')
      // dispatch(getProductsSuccess(response.data))
      return response.data;
    } catch (err) {
      return rejectWithValue('errore getting products!')
    }
  }
)


export const addProduct = (product: Omit<Product, 'id' | 'visibility'>): AppThunk => async (dispatch) => {
  try {

    const res = await axios.post<Product>('http://localhost:3001/products', {
      ...product,
      visibility: false
    })
    dispatch(addProductSuccess(res.data))
  } catch(err) {
    dispatch(setError('Product add failed'))
    // dispatch(setError())
  }
}

export function deleteProduct(id: number): AppThunk {
  return function (dispatch) {
    axios.delete('http://localhost:3001/products/' + id)
      .then(() => {
        dispatch(deleteProductSuccess(id))
      })
  }
}

