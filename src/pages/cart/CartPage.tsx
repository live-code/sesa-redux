import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { useAppDispatch } from '../../App';
import { deleteFromCart } from '../../core/store/cart/cart.actions';
import { selectCart, selectTotalCartCost } from '../../core/store/cart/cart.selectors';

const CartPage = () => {
  const cart = useSelector(selectCart)
  const dispatch = useAppDispatch();
  const cartTotalCost = useSelector(selectTotalCartCost);

  return <div>
    <h1>Cart Summery</h1>
    {
      cart.map(cartItem => <li key={cartItem.id} className="d-flex">
        <div>
          {cartItem.item.title}
          <i
            className="fa fa-trash"
            onClick={() => dispatch(deleteFromCart(cartItem.id))}
          ></i>
        </div>

        <div className="d-flex">
          {cartItem.qty}
        </div>
      </li>
      )
    }

    <hr/>
    Total: € {cartTotalCost}
    <NavLink to="/order" className="btn btn-primary">CHECKOUT</NavLink>

  </div>
};
export default CartPage;
