import { useState } from 'react';
import {
  useDeleteProductMutation,
  useGetProductByIdQuery,
  useGetProductsQuery,
  useGetProductsSMallQuery
} from './store/shop.api';

const Shop2Page = () => {
  const [id, setId] = useState(1)
  const { data: product, error: productError } = useGetProductByIdQuery(id);
  const { data: products, error: productsError, isLoading } = useGetProductsSMallQuery();

  const [
    deleteProduct,
    { isLoading: isDeleteLoading, isError }
  ] = useDeleteProductMutation();

  return <div>
    Shop2Page
    {productError && <pre>Ahia! server problems</pre>}
    {productsError && <pre>Ahia! server problems</pre>}

    <button onClick={() => setId(1)}>1</button>
    <button onClick={() => setId(2)}>2</button>
    <button onClick={() => setId(3)}>3</button>

    {isLoading && <pre>loading....</pre>}
    <pre>PRODUCT: {JSON.stringify(product, null, 2)}</pre>
    <pre>
      {
        products?.map(p => {
          return <li key={p.id}>
            {p.name}
            <i className="fa fa-trash"
               onClick={() => deleteProduct(p.id)}></i>

          </li>
        })
      }</pre>

    <pre>{JSON.stringify(products, null, 2)}</pre>
  </div>
};
export default Shop2Page;
