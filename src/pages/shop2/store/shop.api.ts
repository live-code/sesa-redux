import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { Product } from '../../../model/product';

export const catalogApi = createApi({
  reducerPath: 'catalogRTKQUERY',
  baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:3001'}),
  // refetchOnMountOrArgChange: true,
  tagTypes: ['Products', 'Product'],
  endpoints: build => ({
    getProducts: build.query<Product[], void>({
      query: () => '/products',
      keepUnusedDataFor: 3,
      providesTags: ['Products']
    }),
    getProductsSMall: build.query<{ id: number, name: string }[], void>({
      query: () => '/products',
      transformResponse: (res: Product[]) => {
        return res.map(p => {
          return { id: p.id, name: p.title}
        })
      },
      providesTags: ['Products']
    }),
    getProductById: build.query<Product[], number>({
      query: (id) => `/products/${id}`,
      providesTags: ['Product']
    }),
    deleteProduct: build.mutation({
      query: (id) => ({
        url: `/products/${id}`,
        method: 'DELETE'
      }),
      invalidatesTags: ['Products']
    })
  })
})

export const {
  useGetProductsQuery,
  useGetProductsSMallQuery,
  useGetProductByIdQuery,
  useDeleteProductMutation,
  usePrefetch
} = catalogApi
