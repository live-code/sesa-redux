export type Material = 'wood' | 'plastic';

export interface CounterConfig {
  itemsPerPallet: number;
  material: Material
}
