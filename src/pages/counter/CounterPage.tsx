import { useMemo, useState } from 'react';
import { Root } from 'react-dom/client';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App';
import { changeCounterConfig, changeItemsPerPallet, changeMaterial } from './store/counter-config.actions';
import { decrement, increment, reset } from './store/counter/counter.actions';
import { selectCounter, selectTotalPallets } from './store/counter/counter.selectors';


export const CounterPage = () => {
  const counter = useSelector(selectCounter)
  const totalPallets = useSelector(selectTotalPallets)
  const dispatch = useDispatch();

  return <div>
    <h1>Shipping</h1>
    <div>Total Products: {counter}</div>
    <div>Total Pallets: {totalPallets} </div>

    <button onClick={() => dispatch(decrement(5)) } >-</button>
    <button onClick={() => dispatch(increment(10)) } >+</button>
    <button onClick={() => dispatch(reset()) } >reset</button>
    <hr/>
    <button onClick={() => dispatch(changeMaterial('wood'))} >wood</button>
    <button onClick={() => dispatch(changeMaterial('plastic'))} >plastic</button>
    <button onClick={() => dispatch(changeItemsPerPallet(5))} >5 items</button>
    <button onClick={() => dispatch(changeItemsPerPallet(10))} >10 items</button>
    <button onClick={() => dispatch(changeCounterConfig({  material: 'wood', itemsPerPallet: 5 }))} >wood / 5</button>
    <button onClick={() => dispatch(changeCounterConfig({  material: 'plastic', itemsPerPallet: 10 }))} >Plastic / 10</button>
  </div>
};
