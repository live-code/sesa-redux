import { combineReducers } from '@reduxjs/toolkit';
import { counterConfigReducer } from './counter-config.reducer';
import { counterReducer } from './counter/counter.reducer';

export const counterReducers = combineReducers({
  value: counterReducer,
  config: counterConfigReducer
})
