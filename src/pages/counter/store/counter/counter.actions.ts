import { createAction } from '@reduxjs/toolkit';

export const increment = createAction<number>('increment')
export const decrement = createAction<number>('decrement')
export const reset = createAction('reset')

/*
export function decrement(value: number) {
  return  {
    type: 'decrement',
    payload: value
  }
}
*/
