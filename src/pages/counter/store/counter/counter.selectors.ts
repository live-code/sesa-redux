import { RootState } from '../../../../App';

export const selectCounter = (state: RootState) => state.counter.value
export const selectTotalPallets = (state: RootState) => Math.ceil(state.counter.value / state.counter.config.itemsPerPallet)
