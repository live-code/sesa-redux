import { createAction } from '@reduxjs/toolkit';
import { CounterConfig, Material } from '../model/counter-config';

export const changeMaterial = createAction<Material>('change material');
export const changeItemsPerPallet = createAction<number>('change item per pallet');
export const changeCounterConfig = createAction<Partial<CounterConfig>>('change counter config')
