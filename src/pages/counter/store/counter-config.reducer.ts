import { createReducer } from '@reduxjs/toolkit';
import { CounterConfig } from '../model/counter-config';
import { changeCounterConfig, changeItemsPerPallet, changeMaterial } from './counter-config.actions';

const initialState: CounterConfig = { material: 'wood', itemsPerPallet: 5};

export const counterConfigReducer = createReducer(initialState, builder =>
  builder
    // .addCase(changeMaterial, (state, action) =>  ({ ...state, material: action.payload }))
    .addCase(changeMaterial, (state, action) =>  {
      state.material = action.payload
    })
    .addCase(changeItemsPerPallet, (state, action) =>  {
      state.itemsPerPallet = action.payload
    })
    .addCase(changeCounterConfig, (state, action) =>
                                  ({ ...state, ...action.payload }))
)
